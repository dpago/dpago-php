<?php

namespace Dpago;

use Dpago\Controllers\LinkController;
use Dpago\Controllers\PlatformsController;
use Dpago\Controllers\TransactionController;

class Dpago
{

    public $platforms;
    public $link;
    public $transactions;

    public function __construct($commerceId, $commerceToken)
    {
        $this->platforms = new PlatformsController($commerceId);
        $this->link = new LinkController($commerceId);
        $this->transactions = new TransactionController(
            $commerceId,
            $commerceToken
        );
    }
}
