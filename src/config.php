<?php

// Verifica si la constante FRONT_BASE_URL no está definida antes de definirla
if (!defined('FRONT_BASE_URL')) {
    define('FRONT_BASE_URL', "https://pago.dpago.com");
}

// Verifica si la constante BACK_BASE_URL no está definida antes de definirla
if (!defined('BACK_BASE_URL')) {
    define('BACK_BASE_URL', "https://api.dpago.com");
}
