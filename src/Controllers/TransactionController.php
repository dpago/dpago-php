<?php

namespace Dpago\Controllers;

include __DIR__ . '/../config.php';

use GuzzleHttp\Client;

class TransactionController
{
    public  $commerceToken;
    public $commerceId;

    public function __construct($commerceId, $commerceToken)
    {
        $this->commerceId = $commerceId;
        $this->commerceToken = $commerceToken;
    }

    /**
     * Crea un pago a través de cualquier plataforma
     * @param int $amount Monto que se va a cobrar
     * @param string $description Descripción del cobro realizada por el comercio
     * @param string $reference Referencia de pago creada por el comercio
     * @param int $platformId ID del medio de pago
     * @param bool $withInvoice Indica si se solicitara factura de la transacción
     * @param string $currency Moneda en la que se va a cobrar (Así la moneda se coloque en USD, el precio amount debe ir en Gs.)
     * @param array $customer Arreglo asociativo que representa los datos del cliente.
     *                        Los siguientes elementos están disponibles:
     *                        - "phone" (string): El número de teléfono del cliente.
     *                        - "email" (string): El correo electrónico del cliente.
     *                        - "name" (string): El nombre completo del cliente.
     *                        - "identification" (string): La identificación del cliente (por ejemplo, cédula).
     * @return array Link de pago junto a su ID generado dentro de Dpago
     */
    public function create(
        int $amount,
        string $description,
        string $reference,
        int $platformId,
        bool $withInvoice,
        string $currency = 'PYG',
        array $customer
    ) {
        try {
            $token = hash('sha256', $reference . strval($amount) . $this->commerceToken);
            $client = new Client([
                'base_uri' => BACK_BASE_URL,
                'timeout'  => 5.0,
            ]);
            $information = [
                'reference' => $reference,
                'amount' => $amount,
                'description' => $description,
                'platformId' => $platformId,
                'withInvoice' => $withInvoice,
                'currency' => $currency,
                'commerceId' => $this->commerceId,
                'type' => "common",
                "customer" => [
                    "phone" => $customer["phone"],
                    "email" => $customer["email"],
                    "name" => $customer["name"],
                    "identification" => $customer['identification']
                ],
                'token' => $token
            ];
            $response = $client->request('POST', "/transactions", ['json' => $information]);
            if ($response->getStatusCode() == '200') {
                $json = (string) $response->getBody();
                $json = json_decode($json, true);
                return $json;
            }

            $body = $response->getBody();
            $data = json_decode($body, true);

            return $data;
        } catch (\Exception $e) {
            echo 'Error',  $e->getMessage(), "\n";
        }
    }
}
