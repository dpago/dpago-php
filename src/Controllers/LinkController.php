<?php

namespace Dpago\Controllers;

include __DIR__ . '/../config.php';

use GuzzleHttp\Client;

class LinkController
{
    public $commerceId;

    public function __construct($commerceId)
    {
        $this->commerceId = $commerceId;
    }

    /**
     * Realiza un pago a través del link de pago de Dpago
     * @param int $amount Monto que se va a cobrar
     * @param string $reference Referencia de pago creada por el comercio
     * @param string $description Descripción del cobro realizada por el comercio
     * @param string $currency Moneda en la que se va a cobrar (Así la moneda se coloque en USD, el precio amount debe ir en Gs.)
     * @return array Link de pago junto a su ID generado dentro de Dpago
     */
    public function create(
        int $amount,
        string $description,
        string $currency = 'PYG'
    ) {
        try {
            $client = new Client([
                'base_uri' => BACK_BASE_URL,
                'timeout'  => 5.0,
            ]);
            $trasantionInfo = [
                'amount' => $amount,
                'commerceId' => $this->commerceId,
                'description' => $description,
                'currency' => $currency,
                'type' => 'link'
            ];
            $response = $client->request('POST', "/links", ['json' => $trasantionInfo]);
            if ($response->getStatusCode() == '200') {
                $json = (string) $response->getBody();
                $json = json_decode($json, true);
                return $json;
            }
            $body = $response->getBody();
            $data = json_decode($body, true);
            $link = FRONT_BASE_URL . "/link/" . $data["reference"];
            return [
                'redirectUrl' => $link,
                'reference' => $data["id"]
            ];
        } catch (\Exception $e) {
            echo 'Error',  $e->getMessage(), "\n";
        }
    }
}
