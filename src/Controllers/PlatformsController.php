<?php

namespace Dpago\Controllers;

include __DIR__ . '/../config.php';

use GuzzleHttp\Client;

class PlatformsController
{
    public $commerceId;

    public function __construct($commerceId)
    {
        $this->commerceId = $commerceId;
    }

    /**
     * Obtiene el listado de plataformas de pago, los habilitados y deshabilitados del comercio
     */
    public function get()
    {
        try {
            $client = new Client([
                'base_uri' => BACK_BASE_URL,
                'timeout'  => 5.0,
            ]);
            $response = $client->request('GET', "/commerces/$this->commerceId/platforms");
            if ($response->getStatusCode() == '200') {
                $json = (string) $response->getBody();
                $json = json_decode($json, true);
                return $json;
            }
            $body = $response->getBody();
            return json_decode($body, true);;
        } catch (\Exception $e) {
            echo 'Error',  $e->getMessage(), "\n";
        }
    }
}
